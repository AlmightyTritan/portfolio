<?php

/**
 * ProjectPage
 *
 * @since May 25 2017
 */
class ProjectPage extends Page {

    /**
     * This function will return a thumbnail version of the featured image.
     * If there is no featured image it will return false;
     *
     * @since May 25 2017
     */
    public function thumbnail() {
        // If the image exists
        if ($this->featuredImage()->toFile()->url()) {
            return thumb($this->featuredImage()->toFile(), [
                'width' => 300,
                'height' => 300,
            ]);
        }

        // If that's not the case return false
        return false;
    }
}
